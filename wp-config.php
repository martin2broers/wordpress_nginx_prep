<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin1234');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R@}B/Il0v?zc~;9un?ggRT(#,J.Ghg@[yo@WWcnT dc}~9jdg0vjIs%R0iML:yMJ');
define('SECURE_AUTH_KEY',  '=U6]D }+v~$C[0cwEK4&glrl>Y[zR8CtGk<#<)~(@%pVX.x43|%ag)K~9jPG]pmC');
define('LOGGED_IN_KEY',    'V=gxOvT)<tZJ<FO8<lr2{xxXr`R|p4i)jDUA+sWNWkItQ%hW.>K}xgg;arb&0m{v');
define('NONCE_KEY',        '[s?W4V%,e kw 8471Wts!LsYV?u>pI9N2W]Y0{wW9U5m@Vch]7s6R} a6&vv=|^b');
define('AUTH_SALT',        'ZlMfO!{CJ6jo;kp~JF?izHZxA6W*<gL-te2/JcM-jj<g&~uv6DTnn9QH7{T^4tON');
define('SECURE_AUTH_SALT', '}xwAuz7fI|uR@mX6#!qir)?wpyDg6$;20l6%6qa1nhV41+7z ^Q1j<D,*ZBkb:bI');
define('LOGGED_IN_SALT',   '#cg?RNSF$gZjStT!2hqF/$_ELyS2P7i}HWi3OrZt3#Ap #FR5~%2sSQr*j9)X8t ');
define('NONCE_SALT',       '<BNjnAnjGYC2h>ZK)Gpv518C_|Yd/a`{5F8;~PfU1J%}epY%BV}GS[M ,1Awl.U!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');